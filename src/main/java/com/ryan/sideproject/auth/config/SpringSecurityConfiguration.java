package com.ryan.sideproject.auth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.server.authorization.token.JwtEncodingContext;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenCustomizer;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import java.util.Set;
import java.util.stream.Collectors;

@EnableWebSecurity
public class SpringSecurityConfiguration {

  @Bean
  SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
    // @formatter:off
    http
            .authorizeRequests(authorizeRequests ->
                    authorizeRequests.anyRequest().authenticated()
            )
            .oauth2ResourceServer().jwt().jwtAuthenticationConverter(this.jwtAuthenticationConverter());
    http.formLogin(Customizer.withDefaults());
    // @formatter:on

    return http.build();
  }

  @Bean
  public UserDetailsService users() {
    // @formatter:off
    UserDetails user = User.withDefaultPasswordEncoder()
        .username("admin")
        .password("123")
        .roles("ADMIN").build();
    // @formatter:on

    return new InMemoryUserDetailsManager(user);
  }

 // @Bean
//  @SuppressWarnings("unused")
//  OAuth2TokenCustomizer<JwtEncodingContext> jwtCustomizer() {
//    return context -> {
//      //JoseHeader.Builder headers = context.getHeaders();
//      JwtClaimsSet.Builder claims = context.getClaims();
//
//      Authentication principal = context.getPrincipal();
//      Set<String> authorities = principal.getAuthorities().stream()
//              .map(GrantedAuthority::getAuthority)
//              .collect(Collectors.toSet());
//      claims.claim("authorities", authorities);
//    };
//  }

  private JwtAuthenticationConverter jwtAuthenticationConverter() {
    // create a custom JWT converter to map the "roles" from the token as granted authorities
    JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
    jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName("roles");
    jwtGrantedAuthoritiesConverter.setAuthorityPrefix("ROLE___");
    JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
    jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
    return jwtAuthenticationConverter;
  }

}
